/*
    Pipeline job for Module without Bakefile with following parameters :
    1. url(String)             - module url either to git repo or direct download link
    2. nsVersion(String)       - ns-3 version required for the module
    3. tag(String)             - Tag of release if method is git
    4. method(String)          - Method of module retrieval (git or direct)
    5. id(String)              - Id of the appstore build object
*/

pipeline {
   agent any

   stages {
      stage('cleanup') {
          steps {
            script {
                /*
                    Here appStoreSecret is aJenkins creadential storing AppStore REST API token.
                    url string should point to ns-3 appstore API endpoint. 
                */
                withCredentials([string(credentialsId: 'appStoreSecret', variable: 'token')]) {
                    final String url = "https://apps.nsnam.org/backend/api/build/${id}/"
                    final String content = "\"Content-Type:application/json\""
                    final String token = "\"Authorization: Token ${token}\""
                    final String fields = "'{\"build_id\": ${BUILD_ID}, \"url\": \"${BUILD_URL}\"}'"
                    final String response = sh(script: "curl -X PATCH -H ${token} -H ${content} -d ${fields} $url", returnStdout: true).trim()
                    echo response
                }
            }
            cleanWs()
          }
      }
      stage('main') {
            steps {
                /* 
                    Clone and run module_without_bakefile bash script with pipeline parameters.
                    Should be changed according to location of the bash script.
                */
                sh 'git clone https://gitlab.com/shivamanipatil/test-project/snippets/1990173.git'
                sh '/bin/sh -xe 1990173/script.sh ${method} ${url} ${nsVersion} ${tag}'
         }
      }
   }
   post {
         success {  
            script {
                /*
                    Here appStoreSecret is aJenkins creadential storing AppStore REST API token.
                    url string should point to ns-3 appstore API endpoint. 
                */
                withCredentials([string(credentialsId: 'appStoreSecret', variable: 'token')]) {
                    final String url = "https://apps.nsnam.org/backend/api/build/${id}/"
                    final String content = "\"Content-Type:application/json\""
                    final String token = "\"Authorization: Token ${token}\""
                    final String fields = "'{\"status\": 1}'"
                    final String response = sh(script: "curl -X PATCH -H ${token} -H ${content} -d ${fields} $url", returnStdout: true).trim()
                    echo response
                }
            }
         }  
         failure {  
            script {
                /*
                    Here appStoreSecret is aJenkins creadential storing AppStore REST API token.
                    url string should point to ns-3 appstore API endpoint. 
                */
                withCredentials([string(credentialsId: 'appStoreSecret', variable: 'token')]) {
                    final String url = "https://apps.nsnam.org/backend/api/build/${id}/"
                    final String content = "\"Content-Type:application/json\""
                    final String token = "\"Authorization: Token ${token}\""
                    final String fields = "'{\"status\": 0}'"
                    final String response = sh(script: "curl -X PATCH -H ${token} -H ${content} -d ${fields} $url", returnStdout: true).trim()
                    echo response
                }
            }
         }  
         aborted {
            script {
                /*
                    Here appStoreSecret is aJenkins creadential storing AppStore REST API token.
                    url string should point to ns-3 appstore API endpoint. 
                */
                withCredentials([string(credentialsId: 'appStoreSecret', variable: 'token')]) {
                    final String url = "https://apps.nsnam.org/backend/api/build/${id}/"
                    final String content = "\"Content-Type:application/json\""
                    final String token = "\"Authorization: Token ${token}\""
                    final String fields = "'{\"status\": 3}'"
                    final String response = sh(script: "curl -X PATCH -H ${token} -H ${content} -d ${fields} $url", returnStdout: true).trim()
                    echo response
                }
            }
         }
   }
   
}
